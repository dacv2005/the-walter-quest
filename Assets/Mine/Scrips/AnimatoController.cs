﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatoController : MonoBehaviour {
    [SerializeField]
    float jumpForce;
    Animator anim;
    Rigidbody rb;
    bool ground, attacking;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update() {

        anim.SetBool("Ground", ground);
        anim.SetBool("Attacking", attacking);


		if (Input.GetKeyDown(KeyCode.G) && attacking == false) {
			anim.SetTrigger("kick");
            attacking = true;
		}
        if (Input.GetKeyDown(KeyCode.H) && attacking == false){
            anim.SetTrigger("Punch");
            attacking = true;
        }
		if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
		{
			anim.SetBool("Running", true);
		}
		else
		{
			anim.SetBool("Running", false);
		}
        if (ground){
            if (Input.GetKeyDown(KeyCode.Space)){
                anim.SetTrigger("Jump");
            }
        }
	}
    private void OnCollisionStay(Collision coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            ground = true;
        }
        else
        {
            ground = false;
        }
    }
    void OnCollisionExit(Collision drt)
    {
        if (drt.gameObject.tag == "Ground")
        {
            ground = false;
        }

    }
    void ActualJump()
    {
        print("Jumped");
        rb.AddForce(Vector3.up * jumpForce,ForceMode.Impulse);
    }
}
