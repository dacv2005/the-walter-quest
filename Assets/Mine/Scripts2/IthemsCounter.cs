﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IthemsCounter : MonoBehaviour {
    public int coins;
    public bool sword, axe, bow, shield;
    public int healthPotions;
    public int magicPotions;


    public int coinsLimit, healtPotionsLimit, magicPotionLimit;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (coins > coinsLimit)
            coins = coinsLimit;
        if (healthPotions > healtPotionsLimit)
            healthPotions = healtPotionsLimit;
        if (magicPotions > magicPotionLimit)
            magicPotions = magicPotionLimit;
	}
}
