﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
[RequireComponent(typeof(MineLifer))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class MineTird : MonoBehaviour
{

    //Abiables
    [SerializeField] float walkSpeed, rotSpeed, groundDistance, fuerzaDeSalto;
    [SerializeField] Damager sword, axe;
    [SerializeField] Bow bow;
    //Privates
    float tiempoDeSalto = 0.5f;
    Animator anim;
    MineLifer lifer;
    Transform cam;
    Vector3 camForward, move;
    bool jump, grounded, attacking, alive;
    bool dying;
    bool _sword, _axe, _bow, _shield;
    public bool swordable, axeable, shieldable, bowable;
    int abilities, currentabilitie;
    Rigidbody rb;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        lifer = GetComponent<MineLifer>();
        rb = GetComponent<Rigidbody>();
        alive = true;
        dying = true;
        if (Camera.main != null)
        {
            cam = Camera.main.transform;
        }
    }
    void CheckForAbilities()
    {
        int temp = 0;
        if (swordable)
            temp += 1;
        if (axeable)
            temp += 1;
        if (bowable)
            temp += 1;
        abilities = temp;
    }
    void CheckForAbilitie()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            CheckForAbilities();
            if (abilities == 0)
                return;
            currentabilitie++;
            if (currentabilitie > abilities)
                currentabilitie = 0;
            switch (currentabilitie)
            {
                case 0:
                    _sword = false;
                    _bow = false;
                    _axe = false;
                    break;
                case 1:
                    _sword = true;
                    _axe = false;
                    _bow = false;
                    break;
                case 2:
                    _axe = true;
                    _sword = false;
                    _bow = false;
                    break;
                case 3:
                    _bow = true;
                    _sword = false;
                    _axe = false;
                    break;
            }
        }
    }
    IEnumerator WaitForNoWeapon()
    {
        yield return new WaitForSeconds(1.5f);
        anim.SetLayerWeight(1, 0);
    }
    private void Update()
    {
        anim.SetBool("Axe", _axe);
        anim.SetBool("Sword", _sword);
        anim.SetBool("Bow", _bow);
        anim.SetBool("Shield", _shield);
        if (_axe || _shield || _sword || _bow)
        {
            anim.SetLayerWeight(1, 1);
        }
        else if(anim.GetLayerWeight(1) == 1)
        {
            StartCoroutine(WaitForNoWeapon());
        }

        alive = lifer.alive;
        anim.SetBool("Alive", alive);
        if (alive)
        {
            dying = false;
            Grounder();
            Movement();
            CheckForAttack();
            CheckForJump();
            CheckForAbilitie();
        }
        else
        {
            if (!dying)
            {
                anim.SetTrigger("Death");
                dying = true;
            }
        }
    }
    void Movement()
    {
        anim.SetFloat("YVelocity", rb.velocity.y);
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if (cam != null)
        {
            Vector3 ford = (cam.forward == Vector3.down) ? cam.up : cam.forward;
            camForward = Vector3.Scale(ford, new Vector3(1, 0, 1)).normalized;
            move = v * camForward + h * cam.right;
        }
        if (h != 0 || v != 0)
        {
            transform.LookAt(transform.position + move, Vector3.up);
        }
        Vector3 finalMove = move * walkSpeed;
        Vector3 finalVel = new Vector3(finalMove.x, rb.velocity.y, finalMove.z);
        if (jump)
        {

            tiempoDeSalto -= Time.deltaTime;
            float fuerzaFinalDeSalto = ((tiempoDeSalto * tiempoDeSalto) * fuerzaDeSalto);
            finalVel.y = fuerzaFinalDeSalto;
            if (tiempoDeSalto < 0)
                jump = false;
        }
        else
        {
            finalVel.y = rb.velocity.y;
        }

        rb.velocity = finalVel;
    }
    void Grounder()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundDistance))
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.blue);
            grounded = true;
        }
        else
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.red);
            grounded = false;
        }
        anim.SetBool("Grounded", grounded);
        if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && grounded)
        {
            anim.SetBool("Running", true);
        }
        else
        {
            anim.SetBool("Running", false);
        }
    }
    public void Animate(string trigger)
    {
        if (!attacking)
        {
            anim.SetTrigger(trigger);
        }
    }
    void CheckForAttack()
    {
        if (!attacking && Input.GetButtonDown("Fire1"))
        {
            anim.SetTrigger("Fire1");
            attacking = true;
        }
    }
    void CheckForJump()
    {
        if(grounded && Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("Jump");
            tiempoDeSalto = 0.5F;
        }
    }
    void Jump()
    {
        jump = true;
    }
    void AbleToAttack()
    {
        if (_sword)
            sword.GetComponent<BoxCollider>().enabled = false;
        if (_axe)
            axe.GetComponent<BoxCollider>().enabled = false;
        attacking = false;
    }
    void ActiveWeapon()
    {
        if(_sword)
            sword.GetComponent<BoxCollider>().enabled = true;
        if(_axe)
            axe.GetComponent<BoxCollider>().enabled = true;
        if (_bow)
            bow.FireBow();
    }
    void Hit()
    {

    }
}