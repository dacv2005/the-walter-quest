﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelKill : MonoBehaviour {
    [SerializeField] float timeToDestroy;

    private void Start()
    {
        StartCoroutine(WaitToKill());
    }

    IEnumerator WaitToKill()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}
